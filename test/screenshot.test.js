const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const util = require('util');
const fs = require('fs');
const writeFile = util.promisify(fs.writeFile);
const path = require('path');
const { expect } = require('chai');

async function takeScreenshot(driver, file){
  let image = await driver.takeScreenshot();
  await writeFile(file, image, 'base64');
}

const screen = {
    width: 640,
    height: 480,
};

describe('screenshot', () => {
    const chromeOptions = ['--no-sandbox', '--disable-dev-shm-usage'];
    let driver = new webdriver.Builder()
        .forBrowser('chrome')
        .setChromeOptions(new chrome.Options().addArguments(chromeOptions).headless().windowSize(screen))
        .build();

    it('should have correct display heading', async function () {
        await driver.get('https://www.gitlab.com');
        await takeScreenshot(driver, path.join(process.env.OUTPUT_DIR || '.', 'gitlab.png'));
        this.test.attachments = ['gitlab.png'];
        const headingText = await driver.findElement(webdriver.By.className('title')).getText();
        const expectedHeadingText = "Software. Faster.";

        expect(headingText).to.equal(expectedHeadingText);
    }).timeout(10000);

    after(async () => driver.quit());
});
